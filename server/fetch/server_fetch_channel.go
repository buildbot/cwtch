package fetch

import (
	"cwtch.im/cwtch/protocol"
	"errors"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
)

// CwtchServerFetchChannel implements the ChannelHandler interface for a channel of
// type "im.cwtch.server.fetch" - this implementation only handles server side logic.
type CwtchServerFetchChannel struct {
	Handler CwtchServerFetchHandler
	channel *channels.Channel
}

// CwtchServerFetchHandler defines the interface for interacting with this Channel
type CwtchServerFetchHandler interface {
	HandleFetchRequest() []*protocol.GroupMessage
}

// Type returns the type string for this channel, e.g. "im.ricochet.Cwtch".
func (cc *CwtchServerFetchChannel) Type() string {
	return "im.cwtch.server.fetch"
}

// Closed is called when the channel is closed for any reason.
func (cc *CwtchServerFetchChannel) Closed(err error) {

}

// OnlyClientCanOpen  - for Cwtch channels any side can open
func (cc *CwtchServerFetchChannel) OnlyClientCanOpen() bool {
	return true
}

// Singleton - for Cwtch channels there can only be one instance per direction
func (cc *CwtchServerFetchChannel) Singleton() bool {
	return true
}

// Bidirectional - for Cwtch channels are not bidrectional
func (cc *CwtchServerFetchChannel) Bidirectional() bool {
	return false
}

// RequiresAuthentication - Cwtch channels require hidden service auth
func (cc *CwtchServerFetchChannel) RequiresAuthentication() string {
	return "none"
}

// OpenInbound is the first method called for an inbound channel request.
// If an error is returned, the channel is rejected. If a RawMessage is
// returned, it will be sent as the ChannelResult message.
func (cc *CwtchServerFetchChannel) OpenInbound(channel *channels.Channel, raw *Protocol_Data_Control.OpenChannel) ([]byte, error) {
	cc.channel = channel
	messageBuilder := new(utils.MessageBuilder)
	return messageBuilder.AckOpenChannel(channel.ID), nil
}

// OpenOutbound is the first method called for an outbound channel request.
// If an error is returned, the channel is not opened. If a RawMessage is
// returned, it will be sent as the OpenChannel message.
func (cc *CwtchServerFetchChannel) OpenOutbound(channel *channels.Channel) ([]byte, error) {
	return nil, errors.New("server does not open Fetch channels")
}

// OpenOutboundResult is called when a response is received for an
// outbound OpenChannel request. If `err` is non-nil, the channel was
// rejected and Closed will be called immediately afterwards. `raw`
// contains the raw protocol message including any extension data.
func (cc *CwtchServerFetchChannel) OpenOutboundResult(err error, crm *Protocol_Data_Control.ChannelResult) {
	// NOTE: Should never be called
}

// SendGroupMessages sends a series of group messages to the client.
func (cc *CwtchServerFetchChannel) SendGroupMessages(gms []*protocol.GroupMessage) {
	for _, gm := range gms {
		csp := &protocol.CwtchServerPacket{
			GroupMessage: gm,
		}
		packet, _ := proto.Marshal(csp)
		cc.channel.SendMessage(packet)
	}
}

// Packet is called for each raw packet received on this channel.
func (cc *CwtchServerFetchChannel) Packet(data []byte) {
	csp := &protocol.CwtchServerPacket{}
	err := proto.Unmarshal(data, csp)
	if err == nil {
		if csp.GetFetchMessage() != nil {
			cc.SendGroupMessages(cc.Handler.HandleFetchRequest())
		}
	}
	// If we receive a packet on this channel, close the connection
	cc.channel.CloseChannel()
}
