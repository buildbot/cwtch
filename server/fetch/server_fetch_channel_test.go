package fetch

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"testing"
)

func TestServerFetchChannelAttributes(t *testing.T) {
	cslc := new(CwtchServerFetchChannel)
	if cslc.Type() != "im.cwtch.server.fetch" {
		t.Errorf("cwtch channel type is incorrect %v", cslc.Type())
	}

	if !cslc.OnlyClientCanOpen() {
		t.Errorf("only clients should be able to open im.cwtch.server.fetch channel")
	}

	if cslc.Bidirectional() {
		t.Errorf("im.cwtch.server.fetch should not be bidirectional")
	}

	if !cslc.Singleton() {
		t.Errorf("im.cwtch.server.fetch should be a Singleton")
	}

	if cslc.RequiresAuthentication() != "none" {
		t.Errorf("cwtch channel required auth is incorrect %v", cslc.RequiresAuthentication())
	}

}
func TestServerFetchChannelOpenOutbound(t *testing.T) {
	cslc := new(CwtchServerFetchChannel)
	channel := new(channels.Channel)
	_, err := cslc.OpenOutbound(channel)
	if err == nil {
		t.Errorf("server implementation of im.cwtch.server.fetch should never open an outbound channel")
	}
}

type TestHandler struct {
}

func (th *TestHandler) HandleFetchRequest() []*protocol.GroupMessage {
	gm := &protocol.GroupMessage{
		Ciphertext: []byte("Hello"),
		Spamguard:  []byte{},
	}
	return []*protocol.GroupMessage{gm}
}

func TestServerFetchChannel(t *testing.T) {
	cslc := new(CwtchServerFetchChannel)
	th := new(TestHandler)
	cslc.Handler = th
	channel := new(channels.Channel)
	channel.ID = 1
	closed := false
	channel.CloseChannel = func() {
		closed = true
	}
	gotgm := false
	channel.SendMessage = func([]byte) {
		gotgm = true
	}

	oc := &Protocol_Data_Control.OpenChannel{
		ChannelIdentifier: proto.Int32(1),
		ChannelType:       proto.String(cslc.Type()),
	}

	resp, err := cslc.OpenInbound(channel, oc)
	if err != nil {
		t.Errorf("OpenInbound for im.cwtch.server.Fetch should have succeeded, instead: %v", err)
	}

	control := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(resp[:], control)

	if control.GetChannelResult() != nil {

		fm := &protocol.FetchMessage{}

		csp := &protocol.CwtchServerPacket{
			FetchMessage: fm,
		}

		packet, _ := proto.Marshal(csp)
		cslc.Packet(packet)

		if !gotgm {
			t.Errorf("Did not receive packet on wire as expected in Fetch channel")
		}

		if !closed {
			t.Errorf("Fetch channel should be cosed")
		}

		if !closed {
			t.Errorf("Fetch channel should be closed after incorrect packet received")
		}

	} else {
		t.Errorf("Expected ChannelResult from im.cwtch.server.Fetch, instead: %v", control)
	}

}
