package send

import (
	"cwtch.im/cwtch/protocol"
	"cwtch.im/cwtch/protocol/spam"
	"errors"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"log"
)

// CwtchServerSendChannel implements the ChannelHandler interface for a channel of
// type "im.cwtch.server.send - this implementation only handles server-side logic.
type CwtchServerSendChannel struct {
	// Methods of Handler are called for Cwtch events on this channel
	Handler   CwtchServerSendChannelHandler
	channel   *channels.Channel
	spamguard spam.Guard
}

// CwtchServerSendChannelHandler defines the interface needed to interact with this channel
type CwtchServerSendChannelHandler interface {
	HandleGroupMessage(*protocol.GroupMessage)
}

// Type returns the type string for this channel, e.g. "im.ricochet.Cwtch".
func (cc *CwtchServerSendChannel) Type() string {
	return "im.cwtch.server.send"
}

// Closed is called when the channel is closed for any reason.
func (cc *CwtchServerSendChannel) Closed(err error) {

}

// OnlyClientCanOpen  - for Cwtch channels any side can open
func (cc *CwtchServerSendChannel) OnlyClientCanOpen() bool {
	return true
}

// Singleton - for Cwtch channels there can only be one instance per direction
func (cc *CwtchServerSendChannel) Singleton() bool {
	return true
}

// Bidirectional - for Cwtch channels are not bidrectional
func (cc *CwtchServerSendChannel) Bidirectional() bool {
	return false
}

// RequiresAuthentication - Cwtch channels require hidden service auth
func (cc *CwtchServerSendChannel) RequiresAuthentication() string {
	return "none"
}

// OpenInbound is the first method called for an inbound channel request.
// If an error is returned, the channel is rejected. If a RawMessage is
// returned, it will be sent as the ChannelResult message.
func (cc *CwtchServerSendChannel) OpenInbound(channel *channels.Channel, raw *Protocol_Data_Control.OpenChannel) ([]byte, error) {
	cc.channel = channel
	cc.spamguard.Difficulty = 2
	return cc.spamguard.GenerateChallenge(channel.ID), nil
}

// OpenOutbound is the first method called for an outbound channel request.
// If an error is returned, the channel is not opened. If a RawMessage is
// returned, it will be sent as the OpenChannel message.
func (cc *CwtchServerSendChannel) OpenOutbound(channel *channels.Channel) ([]byte, error) {
	return nil, errors.New("server does not open send channel")
}

// OpenOutboundResult is called when a response is received for an
// outbound OpenChannel request. If `err` is non-nil, the channel was
// rejected and Closed will be called immediately afterwards. `raw`
// contains the raw protocol message including any extension data.
func (cc *CwtchServerSendChannel) OpenOutboundResult(err error, crm *Protocol_Data_Control.ChannelResult) {
	// NOTE: Should never be called
}

// Packet is called for each raw packet received on this channel.
func (cc *CwtchServerSendChannel) Packet(data []byte) {
	csp := &protocol.CwtchServerPacket{}
	err := proto.Unmarshal(data, csp)
	if err == nil {
		if csp.GetGroupMessage() != nil {
			gm := csp.GetGroupMessage()
			ok := cc.spamguard.ValidateChallenge(gm.GetCiphertext(), gm.GetSpamguard())
			if ok {
				cc.Handler.HandleGroupMessage(gm)
			} else {
				log.Printf("[ERROR] Failed to validate spamguard %v\n", gm)
			}
		}
	} else {
		log.Printf("[ERROR] Failed to decode packet on SEND channel %v\n", err)
	}
	cc.channel.CloseChannel()
}
