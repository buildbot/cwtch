package send

import (
	"cwtch.im/cwtch/protocol"
	"cwtch.im/cwtch/protocol/spam"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"testing"
)

type TestHandler struct {
	Received bool
}

func (th *TestHandler) HandleGroupMessage(m *protocol.GroupMessage) {
	th.Received = true
}

func TestServerSendChannelAttributes(t *testing.T) {
	cssc := new(CwtchServerSendChannel)
	if cssc.Type() != "im.cwtch.server.send" {
		t.Errorf("cwtch channel type is incorrect %v", cssc.Type())
	}

	if !cssc.OnlyClientCanOpen() {
		t.Errorf("only clients should be able to open im.cwtch.server.send channel")
	}

	if cssc.Bidirectional() {
		t.Errorf("im.cwtch.server.send should not be bidirectional")
	}

	if !cssc.Singleton() {
		t.Errorf("im.cwtch.server.send should be a Singleton")
	}

	if cssc.RequiresAuthentication() != "none" {
		t.Errorf("cwtch channel required auth is incorrect %v", cssc.RequiresAuthentication())
	}

}
func TestServerSendChannelOpenOutbound(t *testing.T) {
	cssc := new(CwtchServerSendChannel)
	channel := new(channels.Channel)
	_, err := cssc.OpenOutbound(channel)
	if err == nil {
		t.Errorf("server implementation of im.cwtch.server.send should never open an outbound channel")
	}
}

func TestServerSendChannel(t *testing.T) {
	cssc := new(CwtchServerSendChannel)
	th := new(TestHandler)
	cssc.Handler = th
	channel := new(channels.Channel)
	channel.ID = 1
	closed := false
	channel.CloseChannel = func() {
		closed = true
	}

	oc := &Protocol_Data_Control.OpenChannel{
		ChannelIdentifier: proto.Int32(1),
		ChannelType:       proto.String(cssc.Type()),
	}

	resp, err := cssc.OpenInbound(channel, oc)
	if err != nil {
		t.Errorf("OpenInbound for im.cwtch.server.send should have succeeded, instead: %v", err)
	}

	control := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(resp[:], control)

	if control.GetChannelResult() != nil {

		var spamguard spam.Guard
		spamguard.Difficulty = 2

		ce, _ := proto.GetExtension(control.GetChannelResult(), protocol.E_ServerNonce)
		challenge := ce.([]byte)[:]

		sgsolve := spamguard.SolveChallenge(challenge, []byte("Hello"))
		//t.Logf("Solved: %x",  sgsolve)

		gm := &protocol.GroupMessage{
			Ciphertext: []byte("Hello"),
			Signature:  []byte{},
			Spamguard:  sgsolve,
		}

		csp := &protocol.CwtchServerPacket{
			GroupMessage: gm,
		}
		packet, _ := proto.Marshal(csp)

		cssc.Packet(packet)

		if !th.Received {
			t.Errorf("group message should have been received")
		}

		if !closed {
			t.Errorf("im.cwtch.server.send should have been closed after use")
		}
	} else {
		t.Errorf("Expected ChannelResult from im.cwtch.server.send, instead: %v", control)
	}

}

func TestServerSendChannelNoSpamGuard(t *testing.T) {
	cssc := new(CwtchServerSendChannel)
	th := new(TestHandler)
	th.Received = false
	cssc.Handler = th
	channel := new(channels.Channel)
	channel.ID = 1
	closed := false
	channel.CloseChannel = func() {
		closed = true
	}

	oc := &Protocol_Data_Control.OpenChannel{
		ChannelIdentifier: proto.Int32(1),
		ChannelType:       proto.String(cssc.Type()),
	}

	resp, err := cssc.OpenInbound(channel, oc)
	if err != nil {
		t.Errorf("OpenInbound for im.cwtch.server.send should have succeeded, instead: %v", err)
	}

	control := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(resp[:], control)

	if control.GetChannelResult() != nil {

		var spamguard spam.Guard
		spamguard.Difficulty = 2

		ce, _ := proto.GetExtension(control.GetChannelResult(), protocol.E_ServerNonce)
		challenge := ce.([]byte)[:]

		sgsolve := spamguard.SolveChallenge(challenge, []byte("4234"))
		//t.Logf("Solved: %x", sgsolve)

		gm := &protocol.GroupMessage{
			Ciphertext: []byte("hello"),
			Signature:  []byte{},
			Spamguard:  sgsolve,
		}

		csp := &protocol.CwtchServerPacket{
			GroupMessage: gm,
		}
		packet, _ := proto.Marshal(csp)

		cssc.Packet(packet)

		if th.Received == true {
			t.Errorf("group message should not have been received")
		}

		if !closed {
			t.Errorf("im.cwtch.server.send should have been closed after use")
		}

	} else {
		t.Errorf("Expected ChannelResult from im.cwtch.server.send, instead: %v", control)
	}

}
