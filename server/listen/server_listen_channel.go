package listen

import (
	"cwtch.im/cwtch/protocol"
	"errors"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
)

// CwtchServerListenChannel implements the ChannelHandler interface for a channel of
// type "im.cwtch.server.listen" - this implementation only handles server side logic.
type CwtchServerListenChannel struct {
	channel *channels.Channel
}

// Type returns the type string for this channel, e.g. "im.ricochet.Cwtch".
func (cc *CwtchServerListenChannel) Type() string {
	return "im.cwtch.server.listen"
}

// Closed is called when the channel is closed for any reason.
func (cc *CwtchServerListenChannel) Closed(err error) {

}

// OnlyClientCanOpen  - for Cwtch channels any side can open
func (cc *CwtchServerListenChannel) OnlyClientCanOpen() bool {
	return true
}

// Singleton - for Cwtch channels there can only be one instance per direction
func (cc *CwtchServerListenChannel) Singleton() bool {
	return true
}

// Bidirectional - for Cwtch channels are not bidrectional
func (cc *CwtchServerListenChannel) Bidirectional() bool {
	return false
}

// RequiresAuthentication - Cwtch channels require hidden service auth
func (cc *CwtchServerListenChannel) RequiresAuthentication() string {
	return "none"
}

// OpenInbound is the first method called for an inbound channel request.
// If an error is returned, the channel is rejected. If a RawMessage is
// returned, it will be sent as the ChannelResult message.
func (cc *CwtchServerListenChannel) OpenInbound(channel *channels.Channel, raw *Protocol_Data_Control.OpenChannel) ([]byte, error) {
	cc.channel = channel
	messageBuilder := new(utils.MessageBuilder)
	return messageBuilder.AckOpenChannel(channel.ID), nil
}

// OpenOutbound is the first method called for an outbound channel request.
// If an error is returned, the channel is not opened. If a RawMessage is
// returned, it will be sent as the OpenChannel message.
func (cc *CwtchServerListenChannel) OpenOutbound(channel *channels.Channel) ([]byte, error) {
	return nil, errors.New("server does not open listen channels")
}

// OpenOutboundResult is called when a response is received for an
// outbound OpenChannel request. If `err` is non-nil, the channel was
// rejected and Closed will be called immediately afterwards. `raw`
// contains the raw protocol message including any extension data.
func (cc *CwtchServerListenChannel) OpenOutboundResult(err error, crm *Protocol_Data_Control.ChannelResult) {
	// NOTE: Should never be called
}

// SendGroupMessage sends a single group message to the peer
func (cc *CwtchServerListenChannel) SendGroupMessage(gm *protocol.GroupMessage) {
	csp := &protocol.CwtchServerPacket{
		GroupMessage: gm,
	}
	packet, _ := proto.Marshal(csp)
	cc.channel.SendMessage(packet)
}

// Packet is called for each raw packet received on this channel.
func (cc *CwtchServerListenChannel) Packet(data []byte) {
	// If we receive a packet on this channel, close the connection
	cc.channel.CloseChannel()
}
