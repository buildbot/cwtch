package listen

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"testing"
)

func TestServerListenChannelAttributes(t *testing.T) {
	cslc := new(CwtchServerListenChannel)
	if cslc.Type() != "im.cwtch.server.listen" {
		t.Errorf("cwtch channel type is incorrect %v", cslc.Type())
	}

	if !cslc.OnlyClientCanOpen() {
		t.Errorf("only clients should be able to open im.cwtch.server.listen channel")
	}

	if cslc.Bidirectional() {
		t.Errorf("im.cwtch.server.listen should not be bidirectional")
	}

	if !cslc.Singleton() {
		t.Errorf("im.cwtch.server.listen should be a Singleton")
	}

	if cslc.RequiresAuthentication() != "none" {
		t.Errorf("cwtch channel required auth is incorrect %v", cslc.RequiresAuthentication())
	}

}
func TestServerListenChannelOpenOutbound(t *testing.T) {
	cslc := new(CwtchServerListenChannel)
	channel := new(channels.Channel)
	_, err := cslc.OpenOutbound(channel)
	if err == nil {
		t.Errorf("server implementation of im.cwtch.server.listen should never open an outbound channel")
	}
}

func TestServerListenChannel(t *testing.T) {
	cslc := new(CwtchServerListenChannel)
	channel := new(channels.Channel)
	channel.ID = 1
	closed := false
	channel.CloseChannel = func() {
		closed = true
	}
	gotgm := false
	channel.SendMessage = func([]byte) {
		gotgm = true
	}

	oc := &Protocol_Data_Control.OpenChannel{
		ChannelIdentifier: proto.Int32(1),
		ChannelType:       proto.String(cslc.Type()),
	}

	resp, err := cslc.OpenInbound(channel, oc)
	if err != nil {
		t.Errorf("OpenInbound for im.cwtch.server.listen should have succeeded, instead: %v", err)
	}

	control := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(resp[:], control)

	if control.GetChannelResult() != nil {
		gm := &protocol.GroupMessage{
			Ciphertext: []byte("Hello"),
			Signature:  []byte{},
			Spamguard:  []byte{},
		}
		cslc.SendGroupMessage(gm)
		if !gotgm {
			t.Errorf("Did not receive packet on wire as expected in listen channel")
		}

		if closed {
			t.Errorf("listen channel should not be cosed")
		}

		cslc.Packet(nil)

		if !closed {
			t.Errorf("listen channel should be closed after incorrect packet received")
		}

	} else {
		t.Errorf("Expected ChannelResult from im.cwtch.server.listen, instead: %v", control)
	}

}
