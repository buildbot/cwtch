package app

import (
	"cwtch.im/cwtch/connectivity/tor"
	"cwtch.im/cwtch/peer"
	"log"
)

// Application is a facade over a cwtchPeer that provides some wrapping logic.
type Application struct {
	Peer peer.CwtchPeerInterface
}

// NewProfile creates a new cwtchPeer with a given name.
func (app *Application) NewProfile(name string, filename string, password string) error {
	profile := peer.NewCwtchPeer(name, password)
	app.Peer = profile
	err := profile.Save(filename)
	if err == nil {

		_, err := tor.NewTorManager(9050, 9051)
		if err != nil {
			return err
		}

		go func() {
			err := app.Peer.Listen()
			if err != nil {
				log.Panic(err)
			}
		}()
	}
	return err
}

// SetProfile loads an existing profile from the given filename.
func (app *Application) SetProfile(filename string, password string) error {
	profile, err := peer.LoadCwtchPeer(filename, password)
	if err != nil {
		return err
	}
	app.Peer = profile
	if err == nil {

		_, err := tor.NewTorManager(9050, 9051)
		if err != nil {
			return err
		}

		go func() {
			err := app.Peer.Listen()
			if err != nil {
				log.Panic(err)
			}
		}()
	}
	return err
}

// PeerRequest attempts to setup peer relationship with the given onion address.`
func (app *Application) PeerRequest(onion string) {
	app.Peer.PeerWithOnion(onion)
}
