package peer

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"testing"
)

func TestPeerChannelAttributes(t *testing.T) {
	cssc := new(CwtchPeerChannel)
	if cssc.Type() != "im.cwtch.peer" {
		t.Errorf("cwtch channel type is incorrect %v", cssc.Type())
	}

	if cssc.OnlyClientCanOpen() {
		t.Errorf("either side should be able to open im.cwtch.peer channel")
	}

	if cssc.Bidirectional() {
		t.Errorf("im.cwtch.peer should not be bidirectional")
	}

	if !cssc.Singleton() {
		t.Errorf("im.cwtch.server.listen should be a Singleton")
	}

	if cssc.RequiresAuthentication() != "im.ricochet.auth.hidden-service" {
		t.Errorf("cwtch channel required auth is incorrect %v", cssc.RequiresAuthentication())
	}
}

type TestHandler struct {
	Received            bool
	ReceviedGroupInvite bool
}

func (th *TestHandler) ClientIdentity(ci *protocol.CwtchIdentity) {
	if ci.GetName() == "hello" {
		th.Received = true
	}
}

func (th *TestHandler) HandleGroupInvite(ci *protocol.GroupChatInvite) {
	///if ci.GetName() == "hello" {
	th.ReceviedGroupInvite = true
	//}
}

func (th *TestHandler) GetClientIdentityPacket() []byte {
	return nil
}

func TestPeerChannel(t *testing.T) {
	th := new(TestHandler)
	cpc := new(CwtchPeerChannel)
	cpc.Handler = th
	channel := new(channels.Channel)
	channel.ID = 3
	result, err := cpc.OpenOutbound(channel)
	if err != nil {
		t.Errorf("should have send open channel request instead %v, %v", result, err)
	}

	cpc2 := new(CwtchPeerChannel)
	channel2 := new(channels.Channel)
	channel2.ID = 3
	sent := false
	channel2.SendMessage = func(message []byte) {
		sent = true
	}

	control := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(result[:], control)
	ack, err := cpc2.OpenInbound(channel2, control.GetOpenChannel())
	if err != nil {
		t.Errorf("should have ack open channel request instead %v, %v", ack, err)
	}

	ackpacket := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(ack[:], ackpacket)
	cpc.OpenOutboundResult(nil, ackpacket.GetChannelResult())
	if channel.Pending != false {
		t.Errorf("Channel should no longer be pending")
	}

	gm := &protocol.CwtchIdentity{
		Name:             "hello",
		Ed25519PublicKey: []byte{},
	}

	cpp := &protocol.CwtchPeerPacket{
		CwtchIdentify: gm,
	}
	packet, _ := proto.Marshal(cpp)
	cpc.Packet(packet)
	if th.Received == false {
		t.Errorf("Should have sent packet to handler")
	}

	cpc2.SendMessage(packet)
	if sent == false {
		t.Errorf("Should have sent packet to channel")
	}

	gci := &protocol.GroupChatInvite{
		GroupName:      "hello",
		GroupSharedKey: []byte{},
		ServerHost:     "abc.onion",
	}

	cpp = &protocol.CwtchPeerPacket{
		GroupChatInvite: gci,
	}
	packet, _ = proto.Marshal(cpp)
	cpc.Packet(packet)
	if th.ReceviedGroupInvite == false {
		t.Errorf("Should have sent invite packet to handler")
	}
}
