package send

import (
	"cwtch.im/cwtch/protocol"
	"cwtch.im/cwtch/protocol/spam"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"testing"
)

func TestPeerSendChannelAttributes(t *testing.T) {
	cssc := new(CwtchPeerSendChannel)
	if cssc.Type() != "im.cwtch.server.send" {
		t.Errorf("cwtch channel type is incorrect %v", cssc.Type())
	}

	if !cssc.OnlyClientCanOpen() {
		t.Errorf("only clients should be able to open im.cwtch.server.send channel")
	}

	if cssc.Bidirectional() {
		t.Errorf("im.cwtch.server.listen should not be bidirectional")
	}

	if !cssc.Singleton() {
		t.Errorf("im.cwtch.server.listen should be a Singleton")
	}

	if cssc.RequiresAuthentication() != "none" {
		t.Errorf("cwtch channel required auth is incorrect %v", cssc.RequiresAuthentication())
	}

}

func TestPeerSendChannelOpenInbound(t *testing.T) {
	cssc := new(CwtchPeerSendChannel)
	channel := new(channels.Channel)
	_, err := cssc.OpenInbound(channel, nil)
	if err == nil {
		t.Errorf("client implementation of im.cwtch.server.Listen should never open an inbound channel")
	}
}

func TestPeerSendChannelClosesOnPacket(t *testing.T) {
	pfc := new(CwtchPeerSendChannel)
	channel := new(channels.Channel)
	closed := false
	channel.CloseChannel = func() {
		closed = true
	}

	pfc.OpenOutbound(channel)
	pfc.Packet([]byte{})
	if !closed {
		t.Errorf("send channel should close if server attempts to send packets")
	}
}

func TestPeerSendChannel(t *testing.T) {
	pfc := new(CwtchPeerSendChannel)

	channel := new(channels.Channel)
	channel.ID = 3
	success := false

	var sg spam.Guard
	sg.Difficulty = 2

	closed := false
	channel.CloseChannel = func() {
		closed = true
	}

	channel.SendMessage = func(message []byte) {
		packet := new(protocol.CwtchServerPacket)
		proto.Unmarshal(message[:], packet)
		if packet.GetGroupMessage() != nil {
			success = sg.ValidateChallenge(packet.GetGroupMessage().GetCiphertext(), packet.GetGroupMessage().GetSpamguard())
		}
	}
	result, err := pfc.OpenOutbound(channel)
	if err != nil {
		t.Errorf("expected result but also got non-nil error: result:%v, err: %v", result, err)
	}

	challenge := sg.GenerateChallenge(3)
	control := new(Protocol_Data_Control.Packet)
	proto.Unmarshal(challenge[:], control)

	pfc.OpenOutboundResult(nil, control.GetChannelResult())
	if channel.Pending {
		t.Errorf("once opened channel should no longer be pending")
	}

	gm := &protocol.GroupMessage{Ciphertext: []byte("hello")}
	pfc.SendGroupMessage(gm)
	if !success {
		t.Errorf("send channel should have successfully sent a valid group message")
	}

	if !closed {
		t.Errorf("send channel should have successfully closed after a valid group message")
	}

	pfc.Closed(nil)

}
