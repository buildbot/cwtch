package peer

import (
	"testing"
)

func TestCwtchPeerGenerate(t *testing.T) {

	alice := NewCwtchPeer("alice","testpass")
	alice.Save("./test_profile")

	aliceLoaded, err := LoadCwtchPeer("./test_profile","testpass")
	if err != nil || aliceLoaded.GetProfile().Name != "alice" {
		t.Errorf("something went wrong saving and loading profiles %v %v", err, aliceLoaded)
	}

	groupID, _, _ := aliceLoaded.StartGroup("test.server")
	exportedGroup, _ := aliceLoaded.ExportGroup(groupID)
	t.Logf("Exported Group: %v from %v", exportedGroup, aliceLoaded.GetProfile().Onion)

	importedGroupID, err := alice.ImportGroup(exportedGroup)
	group := alice.GetGroup(importedGroupID)
	t.Logf("Imported Group: %v, err := %v %v", group, err, importedGroupID)

}
