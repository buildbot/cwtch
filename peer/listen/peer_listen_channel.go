package listen

import (
	"cwtch.im/cwtch/protocol"
	"errors"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
)

// CwtchPeerListenChannel is the peer implementation of im.cwtch.server.listen
type CwtchPeerListenChannel struct {
	channel *channels.Channel
	Handler CwtchPeerSendChannelHandler
}

// CwtchPeerSendChannelHandler is implemented by peers who want to listen to new messages
type CwtchPeerSendChannelHandler interface {
	HandleGroupMessage(*protocol.GroupMessage)
}

// Type returns the type string for this channel, e.g. "im.ricochet.server.listen".
func (cplc *CwtchPeerListenChannel) Type() string {
	return "im.cwtch.server.listen"
}

// Closed is called when the channel is closed for any reason.
func (cplc *CwtchPeerListenChannel) Closed(err error) {

}

// OnlyClientCanOpen  - for Cwtch server channels can only be opened by peers
func (cplc *CwtchPeerListenChannel) OnlyClientCanOpen() bool {
	return true
}

// Singleton - for Cwtch channels there can only be one instance per direction
func (cplc *CwtchPeerListenChannel) Singleton() bool {
	return true
}

// Bidirectional - for Cwtch channels are not bidrectional
func (cplc *CwtchPeerListenChannel) Bidirectional() bool {
	return false
}

// RequiresAuthentication - Cwtch channels require no auth channels
func (cplc *CwtchPeerListenChannel) RequiresAuthentication() string {
	return "none"
}

// OpenInbound - peers should never respond to open inbound requests from servers
func (cplc *CwtchPeerListenChannel) OpenInbound(channel *channels.Channel, raw *Protocol_Data_Control.OpenChannel) ([]byte, error) {
	return nil, errors.New("client does not receive inbound listen channels")
}

// OpenOutbound sets up a new server listen channel
func (cplc *CwtchPeerListenChannel) OpenOutbound(channel *channels.Channel) ([]byte, error) {
	cplc.channel = channel
	messageBuilder := new(utils.MessageBuilder)
	return messageBuilder.OpenChannel(channel.ID, cplc.Type()), nil
}

// OpenOutboundResult confirms a previous open channel request
func (cplc *CwtchPeerListenChannel) OpenOutboundResult(err error, crm *Protocol_Data_Control.ChannelResult) {
	if err == nil {
		if crm.GetOpened() {
			cplc.channel.Pending = false
		}
	}
}

// Packet is called for each server packet received on this channel.
func (cplc *CwtchPeerListenChannel) Packet(data []byte) {
	csp := &protocol.CwtchServerPacket{}
	err := proto.Unmarshal(data, csp)
	if err == nil {
		if csp.GetGroupMessage() != nil {
			gm := csp.GetGroupMessage()
			// We create a new go routine here to avoid leaking any information about processing time
			// TODO Server can probably try to use this to DoS a peer
			go cplc.Handler.HandleGroupMessage(gm)
		}
	}
}
