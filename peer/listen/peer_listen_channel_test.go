package listen

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"testing"
	"time"
)

type TestHandler struct {
	Received bool
}

func (th *TestHandler) HandleGroupMessage(m *protocol.GroupMessage) {
	th.Received = true
}

func TestPeerListenChannelAttributes(t *testing.T) {
	cssc := new(CwtchPeerListenChannel)
	if cssc.Type() != "im.cwtch.server.listen" {
		t.Errorf("cwtch channel type is incorrect %v", cssc.Type())
	}

	if !cssc.OnlyClientCanOpen() {
		t.Errorf("only clients should be able to open im.cwtch.server.listen channel")
	}

	if cssc.Bidirectional() {
		t.Errorf("im.cwtch.server.listen should not be bidirectional")
	}

	if !cssc.Singleton() {
		t.Errorf("im.cwtch.server.listen should be a Singleton")
	}

	if cssc.RequiresAuthentication() != "none" {
		t.Errorf("cwtch channel required auth is incorrect %v", cssc.RequiresAuthentication())
	}

}
func TestPeerListenChannelOpenInbound(t *testing.T) {
	cssc := new(CwtchPeerListenChannel)
	channel := new(channels.Channel)
	_, err := cssc.OpenInbound(channel, nil)
	if err == nil {
		t.Errorf("client implementation of im.cwtch.server.Listen should never open an inbound channel")
	}
}

func TestPeerListenChannel(t *testing.T) {
	pfc := new(CwtchPeerListenChannel)
	th := new(TestHandler)
	pfc.Handler = th
	channel := new(channels.Channel)
	channel.ID = 3
	result, err := pfc.OpenOutbound(channel)
	if err != nil {
		t.Errorf("expected result but also got non-nil error: result:%v, err: %v", result, err)
	}

	cr := &Protocol_Data_Control.ChannelResult{
		ChannelIdentifier: proto.Int32(3),
		Opened:            proto.Bool(true),
	}

	pfc.OpenOutboundResult(nil, cr)
	if channel.Pending {
		t.Errorf("once opened channel should no longer be pending")
	}

	csp := &protocol.CwtchServerPacket{
		GroupMessage: &protocol.GroupMessage{Ciphertext: []byte("hello"), Signature: []byte{}, Spamguard: []byte{}},
	}
	packet, _ := proto.Marshal(csp)

	pfc.Packet(packet)

	// Wait for goroutine to run
	time.Sleep(time.Second * 1)

	if !th.Received {
		t.Errorf("group message should have been received")
	}

	pfc.Closed(nil)

}
