package fetch

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/wire/control"
	"testing"
	"time"
)

type TestHandler struct {
	Received bool
}

func (th *TestHandler) HandleGroupMessage(m *protocol.GroupMessage) {
	th.Received = true
}

func TestPeerFetchChannelAttributes(t *testing.T) {
	cssc := new(CwtchPeerFetchChannel)
	if cssc.Type() != "im.cwtch.server.fetch" {
		t.Errorf("cwtch channel type is incorrect %v", cssc.Type())
	}

	if !cssc.OnlyClientCanOpen() {
		t.Errorf("only clients should be able to open im.cwtch.server.Fetch channel")
	}

	if cssc.Bidirectional() {
		t.Errorf("im.cwtch.server.fetch should not be bidirectional")
	}

	if !cssc.Singleton() {
		t.Errorf("im.cwtch.server.fetch should be a Singleton")
	}

	if cssc.RequiresAuthentication() != "none" {
		t.Errorf("cwtch channel required auth is incorrect %v", cssc.RequiresAuthentication())
	}

}
func TestPeerFetchChannelOpenInbound(t *testing.T) {
	cssc := new(CwtchPeerFetchChannel)
	channel := new(channels.Channel)
	_, err := cssc.OpenInbound(channel, nil)
	if err == nil {
		t.Errorf("client implementation of im.cwtch.server.Fetch should never open an inbound channel")
	}
}

func TestPeerFetchChannel(t *testing.T) {
	pfc := new(CwtchPeerFetchChannel)
	th := new(TestHandler)
	pfc.Handler = th
	channel := new(channels.Channel)
	channel.ID = 3
	channel.SendMessage = func([]byte) {}
	channel.CloseChannel = func() {}
	result, err := pfc.OpenOutbound(channel)
	if err != nil {
		t.Errorf("expected result but also got non-nil error: result:%v, err: %v", result, err)
	}

	cr := &Protocol_Data_Control.ChannelResult{
		ChannelIdentifier: proto.Int32(3),
		Opened:            proto.Bool(true),
	}

	pfc.OpenOutboundResult(nil, cr)
	if channel.Pending {
		t.Errorf("once opened channel should no longer be pending")
	}

	csp := &protocol.CwtchServerPacket{
		GroupMessage: &protocol.GroupMessage{
			Ciphertext: []byte("hello"), Signature: []byte{}, Spamguard: []byte{},
		},
	}
	packet, _ := proto.Marshal(csp)

	pfc.Packet(packet)

	time.Sleep(time.Second * 2)

	if th.Received != true {
		t.Errorf("group message should not have been received")
	}

	pfc.Closed(nil)

}
