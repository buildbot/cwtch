package storage

import (
	"cwtch.im/cwtch/protocol"
	"cwtch.im/cwtch/server/metrics"
	"os"
	"strconv"
	"testing"
)

func TestMessageStore(t *testing.T) {
	os.Remove("ms.test")
	ms := new(MessageStore)
	counter := metrics.NewCounter()
	ms.Init("ms.test", 100000, counter)
	for i := 0; i < 50000; i++ {
		gm := protocol.GroupMessage{
			Ciphertext: []byte("Hello this is a fairly average length message that we are writing here. " + strconv.Itoa(i)),
			Spamguard:  []byte{},
		}
		ms.AddMessage(gm)
	}
	if counter.Count() != 50000 {
		t.Errorf("Counter should be at 50000 was %v", counter.Count())
	}
	ms.Close()
	ms.Init("ms.test", 100000, counter)
	m := ms.FetchMessages()
	if len(m) != 50000 {
		t.Errorf("Should have been 50000 was %v", len(m))
	}

	counter.Reset()

	for i := 0; i < 100000; i++ {
		gm := protocol.GroupMessage{
			Ciphertext: []byte("Hello this is a fairly average length message that we are writing here. " + strconv.Itoa(i)),
			Spamguard:  []byte{},
		}
		ms.AddMessage(gm)
	}

	m = ms.FetchMessages()
	if len(m) != 100000 {
		t.Errorf("Should have been 100000 was %v", len(m))
	}

	ms.Close()
	os.Remove("ms.test")
}
