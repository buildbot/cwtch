#!/bin/bash

set -e
pwd
go test ${1} -coverprofile=model.cover.out -v ./model
go test ${1} -coverprofile=protocol.spam.cover.out -v ./protocol/spam
go test ${1} -coverprofile=storage.cover.out -v ./storage
go test ${1} -coverprofile=peer.connections.cover.out -v ./peer/connections
go test ${1} -coverprofile=peer.fetch.cover.out -v ./peer/fetch
go test ${1} -coverprofile=peer.listen.cover.out -v ./peer/listen
go test ${1} -coverprofile=peer.peer.cover.out -v ./peer/peer
go test ${1} -coverprofile=peer.send.cover.out -v ./peer/send
go test ${1} -coverprofile=peer.cover.out -v ./peer
go test ${1} -coverprofile=server.fetch.cover.out -v ./server/fetch
go test ${1} -coverprofile=server.listen.cover.out -v ./server/listen
go test ${1} -coverprofile=server.send.cover.out -v ./server/send
go test ${1} -coverprofile=server.metrics.cover.out -v ./server/metrics
go test ${1} -coverprofile=server.cover.out -v ./server
echo "mode: set" > coverage.out && cat *.cover.out | grep -v mode: | sort -r | \
awk '{if($1 != last) {print $0;last=$1}}' >> coverage.out
rm -rf *.cover.out
